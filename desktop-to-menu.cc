/*
    Copyright (C) 2017  Carlos H

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <map>
#include <set>
#include <vector>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fnmatch.h>
#include <wordexp.h>
#include <unistd.h>
#include <argp.h>

#define PROGRAM_NAME "desktop-to-menu"

struct {
  bool system;
  int loglevel;
  bool keep;
  bool noaction;
  char *config_file;
  char *output_dir;
} OPTS;

[[noreturn]]
static
void error (std::string const &message)
{
  std::cerr << PROGRAM_NAME ": ERROR: " << message << std::endl;
  std::exit(1);
}


using mapstr = std::map<std::string, std::string>;
using setstr = std::set<std::string>;
using vecstr = std::vector<std::string>;

static
void log (int level, std::string const &msg) {
  if (level < OPTS.loglevel) {
    std::cerr << PROGRAM_NAME << ": " << msg;
    std::cerr.flush();
  }
}

static
void logln (int level, std::string const &msg) {
  log(level, msg);
  if (level < OPTS.loglevel) {
    std::cerr << std::endl;
  }
}

static
void logendl (int level) {
  if (level < OPTS.loglevel) std::cerr << std::endl;
}

static
void logdone(int level) {
  if (level < OPTS.loglevel) {
    if (OPTS.loglevel > 1) logln(level, "done.");
    else std::cerr << "done." << std::endl;
  }
}

static
long len (std::string const &s)
{
  return static_cast<long>(s.length());
}

static
bool contains (std::string &s, char ch)
{
  return s.find(ch) != std::string::npos;
}

static
std::string &trim (std::string &s)
{
  if (s.length() == 0) return s;
  int i = len(s) - 1;
  while (i >= 0 && std::isspace(s[i])) --i;
  s.erase(i+1);
  i = 0;
  while (i < len(s) && std::isspace(s[i])) ++i;
  s.erase(0, i);
  return s;
}

static
std::string &trim (std::string &&s)
{
  return trim(s);
}

static
std::string shift (std::string &s, char delimiter)
{
  auto pos = s.find(delimiter);
  std::string result(s, 0, pos);
  if (pos == std::string::npos)
    s.clear();
  else
    s.erase(0, pos+1);
  return result;
}

static
mapstr read_desktop_entry (std::string const filename)
{
  mapstr result;
  std::ifstream f(filename);
  std::string line;
  while (std::getline(f, line)) {
    trim(line);
    if (line == "[Desktop Entry]") break;
  }
  while (std::getline(f, line)) {
    trim(line);
    if (line.length() == 0) continue;
    if (line[0] == '[') break;
    std::string key { shift(line, '=') };
    result[trim(key)] = trim(line);
  }
  // fix Exec; delete %x
  std::string::size_type pos;
  std::string &exec = result["Exec"];
  while ((pos = exec.find('%')) != std::string::npos)
    exec.erase(pos, 2);
  trim(exec);

  return result;
}

struct match_rule {
  std::string section;
  vecstr e_matches;
  vecstr p_matches;
  std::vector<vecstr> c_matches;

  int match_score (mapstr &de);

  void print (std::ostream &o) {
    o << "to " << section << " :" << std::endl;
    for (auto &s : e_matches)
      o << "    e(" << s << ")" << std::endl;
    for (auto &s : p_matches)
      o << "    p(" << s << ")" << std::endl;
    for (auto &ss : c_matches) {
      o << "    c(";
      for (auto it = ss.begin(); it != ss.end(); ++it) {
        if (it != ss.begin()) o << ",";
        o << *it;
      }
      o << ")" << std::endl;
    }
  }
};

static
void p_until (char const *&p, char until)
{
  while (*p && *p != until) ++p;
}

static
std::string p_string_until (char const *&p, char until)
{
  char const *start = p;
  p_until(p, until);
  return std::string(start, p-start);
}

int match_rule::match_score (mapstr &de)
{
  constexpr int base = 20000;
  auto const exec = de["Exec"];
  auto pos = exec.rfind('/');
  std::string const last_component {
    pos == std::string::npos ? exec : exec.substr(pos+1)
      };

  for (auto const &s : e_matches) {
    if (s == exec) return base + 4;
    if (s == last_component) return base + 4;
  }
  for (auto const &s : e_matches) {
    if (fnmatch(s.c_str(), exec.c_str(), FNM_PATHNAME) == 0)
      return base + 3;
    if (fnmatch(s.c_str(), last_component.c_str(), FNM_PATHNAME) == 0)
      return base + 3;
  }
  auto const &pkg = de["Package"];
  for (auto const &s : p_matches) {
    if (s == pkg) return base + 2;
  }
  for (auto const &s : p_matches) {
    if (fnmatch(s.c_str(), pkg.c_str(), FNM_PATHNAME) == 0)
      return base + 1;
  }

  // cats:
  std::string &catline = de["Categories"];

  vecstr cats;
  char const *p = catline.c_str();
  while (*p) {
    std::string s = trim(p_string_until(p, ';'));
    if (s.length() > 0)
      cats.push_back(s);
    if (*p) ++p;
  }
  int best_score = 0;
  for (auto const &v : c_matches) {
    std::size_t nmatches = 0;
    std::size_t rightmost = 0;
    bool exact = true;
    for (auto const &s : v) {
      bool matched = false;
      for (std::size_t i = 0; i < cats.size(); ++i) {
        if (cats[i] == s) matched = true;
        if (!matched &&
            fnmatch(s.c_str(), cats[i].c_str(), 0) == 0) {
          matched = true;
          exact = false;
        }
        if (matched) {
          if (i > rightmost) rightmost = i;
          break;
        }
      }
      if (!matched) break;
      ++nmatches;
    }
    int score = nmatches == v.size() ?
      (nmatches-1) * 200 + rightmost + (exact ? 100 : 0)
      : 0;
    if (score > best_score) best_score = score;
  }

  return best_score;
}

struct section_rules {
  std::string default_section;
  std::vector<struct match_rule *> match_rules;

  void print (std::ostream &o) {
    if (default_section.length() > 0) {
      o << "default " << default_section << " ;" << std::endl;
    }
    for (auto r : match_rules) {
      r->print(o);
    }
  }

  void clear () {
    default_section.clear();
    for (auto &p : match_rules) {
      delete p;
      p = nullptr;
    }
    match_rules.clear();
  }

  ~section_rules () {
    clear();
  }

  std::string &section_for (mapstr &de);
};

std::string &section_rules::section_for (mapstr &de)
{
  int best_score = 0;
  struct match_rule *best_rule = nullptr;

  for (auto r : match_rules) {
    int score = r->match_score(de);
    if (score > best_score) {
      best_score = score;
      best_rule = r;
    }
  }
  if (best_rule) return best_rule->section;
  return default_section;
}


// static
// void p_until_any (char const *&p, char const *until)
// {
//   while (*p) {
//     for (char const *q = until; *q; ++q) {
//       if (*p == *q) goto end;
//     }
//     ++p;
//   }
//  end:
// }

// static
// std::string p_string_until_any (char const *&p, char const *until)
// {
//   char const *start = p;
//   p_until_any(p, until);
//   return std::string(start, p-start);
// }

static
void p_space (char const *&p)
{
  while (std::isspace(*p) || *p == '#') {
    if (*p == '#') p_until(p, '\n');
    ++p;
  }
}

static
bool p_over (char const *&p, char ch)
{
  if (*p == ch) {
    ++p;
    return true;
  }
  return false;
}

static
bool p_over_spc (char const *&p)
{
  if (std::isspace(*p)) {
    ++p;
    return true;
  }
  return false;
}

static
bool p_over (char const *&p, char const *str)
{
  char const *q = p;
  while (*q == *str) {
    ++q; ++str;
  }
  if (*str == '\0') {
    p = q;
    return true;
  }
  return false;
}

static
int p_line (char const *p, char const *q)
{
  int nls = 1;
  while (p != q) {
    if (*p == '\n') ++nls;
    ++p;
  }
  return nls;
}

[[noreturn]]
static
void p_error (char const *buf, char const *p, std::string const msg)
{
  error("config (line " + std::to_string(p_line(buf, p)) + "): " + msg);
}

static
struct section_rules read_config (std::ifstream &f)
{
  struct section_rules result;

  f.seekg(0, std::ios_base::end);
  auto endpos = f.tellg();
  f.seekg(0, std::ios_base::beg);
  auto sz = 1 + endpos;
  char *buf = new char[sz];
  f.read(buf, endpos);
  buf[endpos] = '\0';

  std::map<std::string, struct match_rule *> rules;

  char const *p = buf, *save = buf;

  // start parsing
  while (*p) {
    p_space(p);
    save = p;
    if (p_over(p, "default")) {
      if (!p_over_spc(p)) p_error(buf, save, "space expected after 'default'");
      if (result.default_section.length() > 0) {
        p_error(buf, save, "default section defined twice");
      }
      p_space(p);
      result.default_section = trim(p_string_until(p, ';'));
      if (!p_over(p, ';')) p_error(buf, save, "'default' missing ending ';'");
      if (contains(result.default_section, '\n'))
        p_error(buf, save, "runaway default section");
    }
    else if (p_over(p, "to")) {
      if (!p_over_spc(p)) p_error(buf, save, "space expected after 'to'");
      p_space(p);

      std::string section = trim(p_string_until(p, ':'));
      auto it = rules.find(section);
      if (it == rules.end()) {
        struct match_rule *mr = new struct match_rule;
        mr->section = section;
        rules[section] = mr;
      }
      struct match_rule *const r = rules[section];

      if (!p_over(p, ':') || contains(section, '\n'))
        p_error(buf, save, "'to' missing ending ':'");

      while (true) {
        p_space(p);
        save = p;
        if (p_over(p, "e(")) {
          std::string e = trim(p_string_until(p, ')'));
          if (!p_over(p, ')') || contains(e, '\n'))
            p_error(buf, save, "unclosed 'e' rule");
          if (e.length() == 0) p_error(buf, save, "empty 'e' rule");
          r->e_matches.push_back(e);
        }
        else if (p_over(p, "p(")) {
          std::string pr = trim(p_string_until(p, ')'));
          if (!p_over(p, ')') || contains(pr, '\n'))
            p_error(buf, save, "unclosed 'p' rule");
          if (pr.length() == 0) p_error(buf, save, "empty 'p' rule");
          r->p_matches.push_back(pr);
        }
        else if (p_over(p, "c(")) {
          vecstr cs;
          std::string c = trim(p_string_until(p, ')'));
          if (!p_over(p, ')')) p_error(buf, save, "unclosed 'c' rule");
          while (c.length() > 0) {
            std::string h = trim(shift(c, ','));
            if (h.length() == 0) continue;
            if (contains(h, '\n')) p_error(buf, save, "unclosed 'c' rule");
            cs.push_back(h);
          }
          if (cs.size() == 0) p_error(buf, save, "empty 'c' rule");
          r->c_matches.push_back(cs);
        }
        else
          break;
      }
    }
    else p_error(buf, save, "unrecognized keyword");
    p_space(p);
  }
  delete [] buf;

  for (auto &kv : rules) {
    result.match_rules.push_back(kv.second);
  }

  return result;
}


static
mapstr get_packages_for_desktop_entries ()
{
  mapstr result;
  std::string const dirname { "/var/lib/dpkg/info" };
  DIR *d = opendir(dirname.c_str()); if (!d) error("couldn't open " + dirname);
  struct dirent *entry;

  while ((entry = readdir(d))) {
    if (fnmatch("*.list", entry->d_name, 0) != 0)
      continue;
    std::string pkgname(entry->d_name, std::strlen(entry->d_name)-5);
    std::ifstream f(dirname + "/" + entry->d_name);

    std::string line;
    while (getline(f, line)) {
      if (fnmatch("/usr/share/applications/*.desktop", line.c_str(), FNM_PATHNAME) == 0)
        result[std::string(line, line.rfind('/')+1)] = pkgname;
    }
  }
  closedir(d);
  return result;
}

static
std::string expand_path (char const *path)
{
  wordexp_t we;
  if (wordexp(path, &we, WRDE_NOCMD) != 0)
    return path;
  if (we.we_wordc < 1) {
    wordfree(&we);
    return path;
  }
  std::string result{ we.we_wordv[0] };
  wordfree(&we);
  return result;
}

enum class MenuEntryInfo { PACKAGE, FILENAME };

static
setstr get_menu_entries_info (std::string dirname, MenuEntryInfo what)
{
  setstr result;
  DIR *d = opendir(dirname.c_str());
  if (!d) return result;

  struct dirent *entry;
  char const *ignore[] = { "README", "core", ".*", "*~", "*.bak" };
  std::string const pkg_prefix { "?package(" };

  while ((entry = readdir(d))) {
    bool jump_to_next = false;
    for (char const *const pat : ignore) {
      if (fnmatch(pat, entry->d_name, 0) == 0) {
        jump_to_next = true;
        break;
      }
    }
    if (jump_to_next) continue;

    std::ifstream f(dirname + "/" + entry->d_name);
    std::string line;
    while (getline(f, line)) {
      int start = 0;
      while (start < len(line) && std::isspace(line[start]))
        ++start;
      if (line.compare(start, pkg_prefix.length(), pkg_prefix) != 0) {
        continue;
      }
      if (what == MenuEntryInfo::FILENAME) {
        result.insert(entry->d_name);
        continue;
      }
      start += pkg_prefix.length();
      auto end = line.find(')', start);
      if (end == std::string::npos) {
        continue;
      }
      std::string packages = line.substr(start, end-start);
      start = 0;
      do {
        end = packages.find(',', start);
        result.insert(trim(packages.substr(start, end-start)));
        if (end != std::string::npos) start = end+1;
      } while (end != std::string::npos);
    }
  }
  closedir(d);

  return result;
}

static
setstr get_packages_with_menu_entries ()
{
  setstr result;
  char const *dirs[] = { "~/.menu", "/etc/menu", "/usr/lib/menu", "/usr/share/menu", "/usr/share/menu/default" };
  bool skip_first = OPTS.system;
  for (char const *const dir : dirs) {
    if (skip_first) { skip_first = false; continue; }
    std::string dirname = expand_path(dir);

    setstr pkgs_in_dir = get_menu_entries_info(dirname, MenuEntryInfo::PACKAGE);
    logln(1, dirname + ": " + std::to_string(pkgs_in_dir.size()) + " packages w/menu entries found");
    result.insert(pkgs_in_dir.begin(), pkgs_in_dir.end());
  }

  return result;
}

static
std::string make_menu (std::string const &pkg,
                       mapstr &de,
                       std::string const &section)
{
  std::string s = "?package(" + pkg + "):needs=\"";
  if (section == "Window Managers") s.append("wm");
  else if (de["Terminal"] == "true") s.append("text");
  else s.append("X11");
  s.append("\" section=\"" + section + "\" ");

  std::string onlyin = de["OnlyShowIn"];
  if (!onlyin.empty()) {
    if (onlyin.back() == ';') onlyin.pop_back();
    onlyin = "[" + onlyin + "] ";
  }
  s.append("title=\"" + onlyin + de["Name"] + "\" ");
  s.append("command=\"" + de["Exec"] + "\"\n");
  s.append("# Categories=" + de["Categories"] + "\n");

  return s;
}

std::string emptystr;

static
mapstr desktop_to_menu (struct section_rules &rules,
                        mapstr &desktop_packages,
                        setstr &omit_packages)
{
  std::string const dirname { "/usr/share/applications" };
  DIR *d = opendir(dirname.c_str());
  if (!d) error("couldn't read " + dirname);

  mapstr result;

  struct dirent *entry;
  while ((entry = readdir(d))) {
    if (fnmatch("*.desktop", entry->d_name, 0) != 0)
      continue;
    logln(1, "Reading " + dirname + "/" + entry->d_name);
    if (desktop_packages.count(entry->d_name) != 1) {
      logln(-1, std::string("warning: no package found for ") + entry->d_name
            + ", skipping");
      continue;
    }
    std::string &pkg = desktop_packages[entry->d_name];
    logln(1, "  (Package " + pkg + ")");
    if (omit_packages.count(pkg) == 1) {
      logln(1, "  (Omitted: package provides menu entry)");
      continue;
    }

    mapstr de = read_desktop_entry(dirname + "/" + entry->d_name);
    if (de["Type"] != "Application") {
      logln(1, "  (Omitted: not of type Application)");
      continue;
    }
    if (de["Hidden"] == "true"
        || de["NoDisplay"] == "true") {
      logln(1, "  (Omitted: hidden or not displayed)");
      continue;
    }

    de["Package"] = pkg;

    std::string section = rules.section_for(de);
    if (section.length() == 0) {
      logln(-1, std::string("warning: no section matched ") + entry->d_name
            + " and no default section is defined, skipping");
      continue;
    }

    logln(1, "  --> " + section);

    std::string menu_entry = make_menu(pkg, de, section);

    std::string &pkg_entry = result[pkg];
    if (pkg_entry.length() > 0) pkg_entry.append("\n");
    pkg_entry.append(menu_entry);
  }
  closedir(d);

  return result;
}

void save (std::string const &filename, std::string const &content)
{
  std::ofstream out(filename, std::ios_base::out | std::ios_base::trunc);

  if (!out) {
    logln(-1, "warning: couldn't open " + filename);
    return;
  }
  out << content;

  out.close();
}

static
std::ifstream open_config_file ()
{
  std::ifstream f;
  if (OPTS.config_file) {
    f.open(OPTS.config_file);
    if (!f) error(std::string("couldn't open ") + OPTS.config_file);
    return f;
  }

  std::string filename = std::string(PROGRAM_NAME) + ".config";
  if (OPTS.system) {
    f.open("/etc/" + filename);
    if (!f) error("couldn't open /etc/" + filename);
  }
  else {
    char *cfg_home = getenv("XDG_CONFIG_HOME");
    std::string cfg_path;
    if (cfg_home) cfg_path = cfg_home;
    else cfg_path = expand_path("~/.config");
    f.open(cfg_path + "/" + filename);
    if (f) return f;

    f.open(expand_path(("~/." + filename).c_str()));
    if (!f) error("couldn't open config file");
  }

  return f;
}

static
std::string get_output_dir ()
{
  std::string dir;
  if (OPTS.output_dir) dir = OPTS.output_dir;
  else if (OPTS.system) dir = "/var/cache/" PROGRAM_NAME;
  else {
    char *xdg_cache = getenv("XDG_CACHE_HOME");
    if (xdg_cache) { dir = xdg_cache; dir += PROGRAM_NAME; }
    else dir = expand_path("~/.cache/" PROGRAM_NAME);
  }
  struct stat st;
  if (stat(dir.c_str(), &st) != 0) {
    logln(1, "Creating directory " + dir);
    if (!OPTS.noaction && mkdir(dir.c_str(), 0755) != 0) {
      error("couldn't access and/or create output dir " + dir);
    }
  }
  return dir;
}

/** argp **/

char documentation[] =
         "\nCreates menu entries from /usr/share/application/*.desktop files.\n"
         "Previous menu entries in the output directory will be ERASED (use --keep not to do it).\n"
         "After running this program, you can update your window manager's menus by running\n"
         "  update-menus --menufilesdir <output-dir> ...\n\nOptions:\v"
         "Default config file is ~/.config/" PROGRAM_NAME ".config or ~/." PROGRAM_NAME ".config,\n"
         "  or /etc/" PROGRAM_NAME ".config if --system is set.\n"
         "Default output directory is ~/.cache/" PROGRAM_NAME ", or /var/cache/" PROGRAM_NAME " (system).";

struct argp_option options[] = {
  { "config",     'c', "filename",    0, "specify configuration file"   },
  { "output-dir", 'o', "directory",   0, "where to put the menu files"  },
  { "keep",       'k', 0,             0, "keep obsolete menu entries in output dir" },
  { "system",     's', 0,             0, "update the system menu"       },
  { "verbose",    'v', "level",       OPTION_ARG_OPTIONAL, "verbose level: 1 (default) or 2" },
  { "noaction",   'n', 0,             0, "no action, just show what it would've been done" },
  { 0 }
};

static
error_t parse_options (int key, char *arg, struct argp_state *state)
{
  switch (key) {
  case 'c':
    if (strlen(arg) == 0 || arg[strlen(arg)-1] == '/')
      error("invalid config file");
    OPTS.config_file = arg;
    break;
  case 'o':
    OPTS.output_dir = arg;
    break;
  case 'k':
    OPTS.keep = true;
    break;
  case 's':
    OPTS.system = true;
    break;
  case 'v':
    if (!arg) OPTS.loglevel = 1;
    else {
      char *err = arg;
      OPTS.loglevel = strtol(arg, &err, 10);
      if (*err != 0 || OPTS.loglevel < 1 || OPTS.loglevel > 2) {
        error("invalid log level. Use " PROGRAM_NAME " --help to see valid values.");
      }
    }
    break;
  case 'n':
    OPTS.noaction = true;
    break;
  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

struct argp argp = { options, parse_options, 0, documentation };

// static std::string nn (char const *s)
// {
//   if (!s) return std::string("(null)");
//   else return std::string(s);
// }

int main (int argc, char **argv)
{
  argp_parse(&argp, argc, argv, 0, 0, 0);

  //std::cout << "c=" << nn(OPTS.config_file) << ", o=" << nn(OPTS.output_dir) << ", s=" << OPTS.system << std::endl;
  //std::cout << "l=" << OPTS.loglevel << ", n=" << OPTS.noaction << std::endl;

  if (!OPTS.system) {
    char *home = getenv("HOME");
    if (!home || !*home)
      error("$HOME not set");
  }

  std::ifstream config = open_config_file();

  log(0, "Reading config..."); logendl(1);
  struct section_rules rules = read_config(config);
  logdone(0);

  //rules.print(std::cout);

  log(0, "Reading packages..."); logendl(1);
  mapstr df = get_packages_for_desktop_entries();
  logdone(0);

  log(0, "Reading menu entries..."); logendl(1);
  setstr pkgs = get_packages_with_menu_entries();
  logdone(0);

  log(0, "Generating menu entries..."); logendl(1);
  mapstr me = desktop_to_menu(rules, df, pkgs);
  logdone(0);

  std::string menudir = get_output_dir(); //{ expand_path("~/.menu") };
  setstr current = get_menu_entries_info(menudir, MenuEntryInfo::FILENAME);
  log(0, "Updating menu files..."); logendl(1);

  for (auto const &kv : me) {
    std::string const &pkg = kv.first;
    std::string const &contents = kv.second;

    std::string path { menudir + "/" + pkg };

    logln(1, std::string("Writing ") + (OPTS.noaction ? "(not really) " : "") + path);
    if (!OPTS.noaction)
      save(path, contents);
    current.erase(pkg);
  }
  // delete remaining
  if (!OPTS.keep) {
    for (auto const &file : current) {
      std::string path { menudir + "/" + file };
      logln(1, std::string("Deleting ") + (OPTS.noaction ? "(not really) " : "") + path);
      if (!OPTS.noaction)
        std::remove(path.c_str());
    }
  }
  logdone(0);

  return 0;
}
